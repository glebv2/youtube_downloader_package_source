from setuptools import setup

setup(
    name='youtube_downloader',
    version='1.1',
    description='Youtube Downloader by gleb',
    url='https://git.glebmail.xyz/PythonPrograms/youtube_downloader',
    author='gleb',
    packages=['youtube_downloader'],
    author_email='gleb@glebmail.xyz',
    scripts=['bin/youtube_downloader'],
    license='GNU GPL 3',
    install_requires=['pytube',
                      ],
)

